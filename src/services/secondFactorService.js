import Axios from "axios";
import config from "../config/config";

const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

// Use a lookup table to find the index.
const lookup = new Uint8Array(256);
for (let i = 0; i < chars.length; i++) {
    lookup[chars.charCodeAt(i)] = i;
}

const encode = (arraybuffer) => {
    let bytes = new Uint8Array(arraybuffer),
        i, len = bytes.length, base64 = '';

    for (i = 0; i < len; i += 3) {
        base64 += chars[bytes[i] >> 2];
        base64 += chars[((bytes[i] & 3) << 4) | (bytes[i + 1] >> 4)];
        base64 += chars[((bytes[i + 1] & 15) << 2) | (bytes[i + 2] >> 6)];
        base64 += chars[bytes[i + 2] & 63];
    }

    if ((len % 3) === 2) {
        base64 = `${base64.substring(0, base64.length - 1)}`;
    } else if (len % 3 === 1) {
        base64 = `${base64.substring(0, base64.length - 2)}`;
    }

    return base64.replace(/[+]/g, '-').replace(/[/]/g, '_');
}

const decode = (base64) => {
    let bufferLength = base64.length * 0.75,
        len = base64.length, i, p = 0,
        encoded1, encoded2, encoded3, encoded4;

    if (base64[base64.length - 1] === '=') {
        bufferLength--;
        if (base64[base64.length - 2] === '=') {
            bufferLength--;
        }
    }

    const arraybuffer = new ArrayBuffer(bufferLength),
        bytes = new Uint8Array(arraybuffer);

    for (i = 0; i < len; i += 4) {
        encoded1 = lookup[base64.charCodeAt(i)];
        encoded2 = lookup[base64.charCodeAt(i + 1)];
        encoded3 = lookup[base64.charCodeAt(i + 2)];
        encoded4 = lookup[base64.charCodeAt(i + 3)];

        bytes[p++] = (encoded1 << 2) | (encoded2 >> 4);
        bytes[p++] = ((encoded2 & 15) << 4) | (encoded3 >> 2);
        bytes[p++] = ((encoded3 & 3) << 6) | (encoded4 & 63);
    }

    return arraybuffer;
}

const base64decode = (str) => {
    let output = str.replace(/-/g, '+').replace(/_/g, '/');
    switch (output.length % 4) {
        case 0:
            break;
        case 2:
            output += '==';
            break;
        case 3:
            output += '=';
            break;
        default:
            throw 'Illegal base64url string!';
    }
    return window.atob(output);
}

const getConfirmationObject = (rpId, credentialId, challenge) => {
    return {
        publicKey: {
            rpId: rpId,
            allowCredentials: [{
                type: 'public-key',
                id: Uint8Array.from(base64decode(credentialId), c => c.charCodeAt(0)).buffer
            }],
            challenge: decode(challenge),
            timeout: 50000
        }
    };
}

const doWebauthnThings = async (confirmationObject) => {
    return await navigator.credentials.get(confirmationObject)
        .then(rawAssertion => {
            const dataToLogin = {
                clientDataJSON: encode(rawAssertion.response.clientDataJSON),
                signature: encode(rawAssertion.response.signature),
                authenticatorData: encode(rawAssertion.response.authenticatorData)
            };
            return dataToLogin;
        })
}

const attestateWebauthn = async (publicKeyCredentialCreationOptions) => {
    publicKeyCredentialCreationOptions.challenge = decode(publicKeyCredentialCreationOptions.challenge);
    publicKeyCredentialCreationOptions.user.id = decode(publicKeyCredentialCreationOptions.user.id);

    return await navigator.credentials.create({
        publicKey: publicKeyCredentialCreationOptions
    });
}

let secondFactors = [];

export const getSecondFactorsList = () => {
    return secondFactors;
}

export const updateSecondFactorsList = (newSecondFactors) => {
    secondFactors = newSecondFactors;
}

const requestConfirmationToken = async (personId, secondFactorId, securityOperationType, hash, successCallback, errorCallback, httpErrorCallback) => {
    const requestTokenParams = new URLSearchParams();
    requestTokenParams.append('personId', personId);
    requestTokenParams.append('operation', securityOperationType);
    requestTokenParams.append('secondFactorId', secondFactorId);
    requestTokenParams.append('hash', hash);
    await Axios.post(config.privateApiUrl + 'requestSecondFactorToken', requestTokenParams)
        .then(response => {
            if (response.data.success === 1) {
                successCallback(response.data.return.token);
            } else {
                console.log("requestSecondFactorToken failed: " + response.data.error);
                errorCallback(response.data.error);
            }
        })
        .catch(error => {
            console.log("requestSecondFactorToken failed: " + error.response);
            httpErrorCallback();
        });
}

const submitSecondFactorToken = async (personId, token, secondFactor, code, successCallback, errorCallback, httpErrorCallback) => {
    const submitTokenParams = new URLSearchParams();
    submitTokenParams.append('personId', personId);
    submitTokenParams.append('token', token);
    submitTokenParams.append('secondFactorId', secondFactor.id);

    switch (secondFactor.type) {
        case "GA":
            submitTokenParams.append('code', code);
            break;
        case "WEBAUTHN":
/*            let result;
            try {
                result = await doWebauthnThings(
                    getConfirmationObject(globals.rpId, confirmationTokenData.credentialId, confirmationTokenData.challenge)
                );
            } catch {
                errorCallback("");
                return;
            }
            submitTokenParams.append('clientDataJSON', result.clientDataJSON);
            submitTokenParams.append('signature', result.signature);
            submitTokenParams.append('authenticatorData', result.authenticatorData);*/
            break;
        default:
            errorCallback("");
            return;
    }

    await Axios.post(config.privateApiUrl + 'submitSecondFactorToken', submitTokenParams)
        .then(response => {
            if (response.data.success === 1) {
                successCallback(response.data.return.token);
            } else {
                console.log("submitSecondFactorToken failed: " + response.data.error);
                errorCallback(response.data.error);
            }
        })
        .catch(error => {
            console.log("submitSecondFactorToken failed: " + error.response);
            httpErrorCallback();
        });
}

const SecondFactorService = {
    requestConfirmationToken: requestConfirmationToken,
    submitSecondFactorToken: submitSecondFactorToken
};
export default SecondFactorService;

/*export const approveSecondFactor = (confirmationToken, params, successCallback) => {
    apiRequests.postCookieRequest('approveAddSecondFactor', params).success(function (data) {
        if (data.success) {
            const addSecondFactorParams = {
                approveToken: params.token,
                verificationToken: confirmationToken
            };
            apiRequests.postCookieRequest('addSecondFactor', addSecondFactorParams).success(function (data) {
                if (data.success) {
                    successCallback && successCallback();
                } else {
                    console.log(data);
                    alert("Failed to verify second factor.");
                }
            }).error(data => alert("Http exception: " + data.error));
        } else {
            console.log(data);
            alert("Failed to verify second factor.");
        }
    }).error(data => alert("Http exception: " + data.error));
}

export const addSecondFactor = (confirmationToken, alias, type, successCallback) => {
    apiRequests.postCookieRequest('requestAddSecondFactor', {alias: alias, type: type}).success(function (data) {
        if (data.success) {
            let params = {
                token: data.return.tokenId
            };
            if (type === 'WEBAUTHN') {
                attestateWebauthn(data.return.webauthnPublicKey).then(pkCred => {
                    console.log(pkCred);
                    params.attestationObject = encode(pkCred.response.attestationObject);
                    params.clientDataJSON = encode(pkCred.response.clientDataJSON);
                    params.credentialId = pkCred.id;
                    approveSecondFactor(confirmationToken, params, successCallback)
                }).catch((e) => {
                    console.log(e);
                    alert("Failed to attestate webauthn token")
                })
            } else {
                alert('Creation of ' + type + ' second factor is not supported yet.')
            }
        } else {
            console.log(data);
            alert("Failed to add second factor.");
        }
    }).error(data => alert("Http exception: " + data.error));
}

export const fetchSecondFactors = (successCallback) => {
    apiRequests.postCookieRequest('checkAuth', {
        "timestamp": Date.now()
    })
        .success(function (data) {
            if (!data.success) {
                alert(data.error);
            } else {
                updateSecondFactorsList(data.return.secondFactors);
                successCallback(data.return.secondFactors);
            }
        }).error(function (error) {
        console.log(error)
    });
}

export const deleteSecondFactor = (token, id, successCallback) => {
    var params = {
        token: token,
        secondFactorId: id
    };

    // TODO
    apiRequests.postCookieRequest('deleteSecondFactor', params)
        .success(function (data) {
            if (data.success) {
                successCallback();
            } else {
                gritterService.addDanger("Unable to delete second factor. Error: " + JSON.stringify(data));
            }
        })
        .error(function (data, err) {
            gritterService.addDanger("Unable to delete second factor. Error: " + JSON.stringify(data) + " " + err);
        });
}
*/