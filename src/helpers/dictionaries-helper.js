import 'react-notifications-component/dist/theme.css';
import Cookies from "js-cookie";
import Axios from "axios";
import config from "../config/config";
import NotificationsHelper from "./notifications-helper";

class DictionariesHelper {
    constructor() {

    }

    getUserStatuses(callback) {
        const params = new URLSearchParams();
        params.append('cookie', Cookies.get('access_token'));
        Axios.post(config.privateApiUrl + 'getUserStatuses', params)
            .then(response => {
                if (response.data.success === 1) {
                    callback(response.data.return);
                } else {
                    console.log("getUserStatuses failed: " + response.data.error);
                    NotificationsHelper.getInstance().showErrorNotification("Oh, no... =(", "Could not get users list: " + response.data.error);
                    callback([]);
                }
            })
            .catch(error => {
                console.log("getUserStatuses failed: " + error.response);
                callback([]);
            });
    }
}

export default new DictionariesHelper();