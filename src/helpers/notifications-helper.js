import { store } from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';

export default class NotificationsHelper {
    static instance = null;

    static getInstance() {
        if (NotificationsHelper.instance == null) {
            NotificationsHelper.instance = new NotificationsHelper();
        }
        return this.instance;
    }

    showNotification(title, message, type, insert, container, animationIn, animationOut, dismissDuration) {
        store.addNotification({
            title: title,
            message: message,
            type: type,
            insert: insert,
            container: container,
            animationIn: animationIn,
            animationOut: animationOut,
            dismiss: {duration: dismissDuration},
            dismissable: {click: true}
        });
    }

    showSuccessNotification(title, message) {
        return this.showNotification(title, message, "success", "top", "top-right",
            ["animated", "fadeIn"], ["animated", "fadeOut"], 3000)
    }

    showErrorNotification(title, message) {
        return this.showNotification(title, message, "danger", "top", "top-right",
            ["animated", "fadeIn"], ["animated", "fadeOut"], 60000)
    }
}