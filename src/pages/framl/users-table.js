import React from 'react';
import Axios from "axios";
import Cookies from 'js-cookie'
import config from "./../../config/config";
import DictionariesHelper from "../../helpers/dictionaries-helper"
import ReactTable from 'react-table';
import {Link} from 'react-router-dom';
import {Panel, PanelHeader} from '../../components/panel/panel';
import 'react-table/react-table.css';
import NotificationsHelper from '../../helpers/notifications-helper.js';

class UsersTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            usSelectItems: [],
            users: [],
            pages: null,
            loading: true,
            filter: {}
        };

        this.defaultSorted = [
            {
                id: "user_id",
                desc: false
            }
        ];

        this.tableColumns = [
            {
                Header: "ID",
                id: "user_id",
                accessor: "id"
            },
            {
                Header: "Type",
                accessor: "type",
                Filter: ({filter, onChange}) =>
                    <select className="form-control"
                            onChange={event => onChange(event.target.value)}
                            style={{width: '100%'}}
                            value={filter ? filter.value : ''}
                    >
                        <option value="">All</option>
                        <option value="INDIVIDUAL">INDIVIDUAL</option>
                        <option value="CORPORATE">CORPORATE</option>
                        <option value="SPECIAL">SPECIAL</option>
                    </select>
            },
            {
                Header: "Status",
                id: "userStatus",
                accessor: "status",
                Filter: ({filter, onChange}) =>
                    <select className="form-control"
                            onChange={event => onChange(event.target.value)}
                            style={{width: '100%'}}
                            value={filter ? filter.value : ''}
                    >
                        <option value="">All</option>
                        {this.state.usSelectItems}
                    </select>
            },
            {
                Header: "Verif. source",
                accessor: "verification_source",
                Filter: ({filter, onChange}) =>
                    <select className="form-control"
                            onChange={event => onChange(event.target.value)}
                            style={{width: '100%'}}
                            value={filter ? filter.value : ''}
                    >
                        <option value="">All</option>
                        <option value="EP">EP</option>
                        <option value="DSX">DSX</option>
                        <option value="NONE">NONE</option>
                    </select>
            },
            {
                Header: "Owner email",
                id: "email",
                accessor: "person.email"
            },
            {
                Header: "Owner name",
                id: "name",
                accessor: "person.firstName",
                Cell: row => (
                    <span>{row.original.person.firstName} {row.original.person.familyName}</span>
                )
            },
            {
                Header: "Country of nationality",
                id: "country",
                accessor: "person.citizenship"
            },
            {
                Header: 'Status',
                accessor: 'enabled',
                Cell: row => (
                    <span>
							<i className="fa fa-circle f-s-7 fa-fw m-r-10 pull-left m-t-5" style={{
                                color: !row.value ? '#ff5b57' : '#00acac',
                                transition: 'all .3s ease'
                            }}/>
                        {row.value ? 'Enabled' : 'Disabled'}
                    </span>
                )
            }
        ]

        this.fetchData = this.fetchData.bind(this);
    }

    componentDidMount() {
        DictionariesHelper.getUserStatuses((statuses) => {
            let items = [];
            statuses.forEach(s => items.push(<option key={s} value={s}>{s}</option>));
            this.setState({usSelectItems: items});
        });
    }

    getStyleByCurrencyType = (type) => {
        switch (type) {
            case "FIAT":
                return "label label-green";
            case "CRYPTO":
                return "label label-pink";
            case "BTC_FORK":
                return "label label-yellow";
            case "ERC20":
                return "label label-info";
            default:
                return "label label-default";
        }
    }

    /*
        toggleReload = () => {
            this.getUsers();
        }*/

    fetchData(state, instance) {
        this.setState({loading: true});

        const token = Cookies.get('access_token');

        const params = new URLSearchParams();
        params.append('cookie', token);
        params.append('offset', state.page + 1);
        params.append('limit', state.pageSize);
        if (state.sorted) {
            params.append('sortedBy', state.sorted.map(d => d.id));
            params.append('isDirectOrder', state.sorted.map(d => (d.desc ? "false" : "true")));
        }

        if (state.filter && state.filter.status) {
            params.append('userStatus', state.filter.status);
        }
        if (state.filtered) {
            // TODO Remove logging
            state.filtered.forEach(f => console.log(f.id + " " + f.value));
            state.filtered.forEach(f => params.append(f.id, f.value));
        }

        Axios.post(config.privateApiUrl + 'getUsers', params)
            .then(response => {
                if (response.data.success === 1) {
                    this.setState({
                        users: response.data.return.items,
                        pages: Math.ceil(response.data.return.count / state.pageSize),
                        loading: false
                    });
                } else {
                    console.log("getUsers failed: " + response.data.error);
                    this.setState({users: []});
                    NotificationsHelper.getInstance().showErrorNotification("Oh, no... =(", "Could not get users list: " + response.data.error);
                }
            })
            .catch(error => {
                console.log("getUsers failed: " + error.response);
                this.setState({users: []});
            });
    }

    render() {
        const {users, pages, loading} = this.state;

        return (
            <div>
                <ol className="breadcrumb float-xl-right">
                    <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                    <li className="breadcrumb-item"><Link to="/framl">FRAML</Link></li>
                    <li className="breadcrumb-item active">Users</li>
                </ol>
                <h1 className="page-header">Users table</h1>
                <Panel>
                    <PanelHeader hideRemoveButton={true} hideCollapseButton={true}>
                        Users
                    </PanelHeader>

                    {/*                    <Select
                        style={{width: "50%", marginBottom: "20px"}}
                        onChange={this.fetchData}
                        value={this.state.filter.status}
                        multi={false}
                        options={this.state.userStatuses.map(s => {
                            return {id: s, value: s, label: s};
                        })}
                    />*/}

                    <ReactTable filterable
                                data={users}
                                pages={pages}
                                loading={loading}
                                columns={this.tableColumns}
                                defaultSorted={this.defaultSorted}
                                className="-highlight"
                                showPagination={true}
                                showPaginationTop={false}
                                showPaginationBottom={true}
                                defaultPageSize={20}
                                pageSizeOptions={[10, 20, 25, 50, 100]}
                                manual
                                onFetchData={this.fetchData}
                    />
                </Panel>
            </div>
        )
    }
}

export default UsersTable;