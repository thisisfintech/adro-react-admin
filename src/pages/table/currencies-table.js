import React from 'react';
import Axios from "axios";
import Cookies from 'js-cookie'
import config from "./../../config/config";
import ReactTable from 'react-table';
import {Link} from 'react-router-dom';
import {Panel, PanelHeader} from './../../components/panel/panel.jsx';
import 'react-table/react-table.css';
import NotificationsHelper from '../../helpers/notifications-helper.js';

class CurrenciesTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currencies: []
        };

        this.defaultSorted = [
            {
                id: "id",
                desc: false
            }
        ];
        this.tableColumns = [
            {
                Header: "ID",
                id: "id",
                accessor: "id"
            },
            {
                Header: "Name",
                accessor: "name"
            },
            {
                Header: "Currency type",
                accessor: "currencyType",
                Cell: row => (<span className={this.getStyleByCurrencyType(row.value)}>{row.value}</span>),
                Filter: ({ filter, onChange }) =>
                    <select className="form-control"
                        onChange={event => onChange(event.target.value)}
                        style={{ width: '100%' }}
                        value={filter ? filter.value : 'all'}
                    >
                        <option value="">All</option>
                        <option value="FIAT">FIAT</option>
                        <option value="CRYPTO">CRYPTO</option>
                        <option value="BTC_FORK">BTC_FORK</option>
                        <option value="ERC20">ERC20</option>
                    </select>
            },
            {
                Header: "Decimal places",
                accessor: "decimalPlaces"
            },
            {
                Header: "Self position",
                accessor: "current_self_position",
                Cell: row => (
                    <span>{row.value} ({row.original.min_self_position} - {row.original.max_self_position})</span>
                )
            },
            {
                Header: 'Status',
                accessor: 'enabled',
                Cell: row => (
                    <span>
							<i className="fa fa-circle f-s-7 fa-fw m-r-10 pull-left m-t-5" style={{
                                color: !row.value ? '#ff5b57' : '#00acac',
                                transition: 'all .3s ease'
                            }}/>
                        {row.value ? 'Enabled' : 'Disabled'}
                    </span>
                )
            }
        ]
    }

    componentDidMount(): void {
        this.getCurrencies();
    }

    getCurrencies = () => {
        const token = Cookies.get('access_token');

        const params = new URLSearchParams();
        params.append('cookie', token);
        Axios.post(config.privateApiUrl + 'getCurrencies', params)
            .then(response => {
                if (response.data.success === 1) {
                    this.setState({currencies: response.data.return.filter(c => c.type === 'LIVE')});
                } else {
                    console.log("getCurrencies failed: " + response.data.error);
                    this.setState({currencies: []});
                    NotificationsHelper.getInstance().showErrorNotification("Oh, no... =(", "Could not get currencies list: " + response.data.error);
                }
            })
            .catch(error => {
                console.log("getCurrencies failed: " + error.response);
                this.setState({currencies: []});
            });
    }

    getStyleByCurrencyType = (type) => {
        switch (type) {
            case "FIAT": return "label label-green";
            case "CRYPTO": return "label label-pink";
            case "BTC_FORK": return "label label-yellow";
            case "ERC20": return "label label-info";
            default: return "label label-default";
        }
    }

    toggleReload = () => {
        this.getCurrencies();
    }

    render() {
        return (
            <div>
                <ol className="breadcrumb float-xl-right">
                    <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                    <li className="breadcrumb-item"><Link to="/settings">Settings</Link></li>
                    <li className="breadcrumb-item active">Currencies</li>
                </ol>
                <h1 className="page-header">Currencies table</h1>
                <Panel>
                    <PanelHeader hideRemoveButton={true} hideCollapseButton={true} toggleReload={this.toggleReload}>
                        Currencies
                    </PanelHeader>
                    <ReactTable filterable data={this.state.currencies} columns={this.tableColumns} defaultPageSize={20}
                                defaultSorted={this.defaultSorted} className="-highlight"/>
                </Panel>
            </div>
        )
    }
}

export default CurrenciesTable;