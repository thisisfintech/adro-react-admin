import React from 'react';
import Axios from "axios";
import Cookies from 'js-cookie'
import {Link, Redirect, withRouter} from 'react-router-dom';
import {PageSettings} from './../../config/page-settings.js';
import config from "./../../config/config";
import SecondFactorService from "./../../services/secondFactorService";
import ReactNotification from 'react-notifications-component';
import NotificationsHelper from '../../helpers/notifications-helper.js';
import DictionariesHelper from "../../helpers/dictionaries-helper";

class LoginV2 extends React.Component {
    static contextType = PageSettings;

    constructor(props) {
        super(props);

        this.state = {
            loginPhase: 'login',
            login: '',
            password: '',
            code: '',
            redirect: false,
            secondFactors: [],
            selectedFactor: '',
            selectedFactorObj: null,
            activeBg: '/assets/img/login-bg/login-bg-17.jpg',
            bg1: true,
            bg2: false,
            bg3: false,
            bg4: false,
            bg5: false,
            bg6: false
        }
        this.selectBg = this.selectBg.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleFirstPhaseSubmit = this.handleFirstPhaseSubmit.bind(this);
        this.handleSecondPhaseSubmit = this.handleSecondPhaseSubmit.bind(this);

        Axios.defaults.headers.post['Accept-Encoding'] = "gzip, deflate, br";
        Axios.defaults.headers.post['Accept-Language'] = "en-GB,en;q=0.9,en-US;q=0.8,ru;q=0.7";

        Axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
        Axios.defaults.headers.post['Host'] = 'bo.dsx.uk';
        Axios.defaults.headers.post['Origin'] = 'http://localhost';

        Axios.defaults.headers.post['Sec-Fetch-Mode'] = 'cors';
        Axios.defaults.headers.post['Sec-Fetch-Site'] = 'cross-site';
    }

    selectBg(e, active, bg) {
        e.preventDefault();

        this.setState(state => ({
            activeBg: bg,
            bg1: (active === 'bg1') ? true : false,
            bg2: (active === 'bg2') ? true : false,
            bg3: (active === 'bg3') ? true : false,
            bg4: (active === 'bg4') ? true : false,
            bg5: (active === 'bg5') ? true : false,
            bg6: (active === 'bg6') ? true : false
        }));
    }

    componentDidMount() {
        this.context.handleSetPageSidebar(false);
        this.context.handleSetPageHeader(false);
    }

    componentWillUnmount() {
        this.context.handleSetPageSidebar(true);
        this.context.handleSetPageHeader(true);
    }

    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});

        if (event.target.name === 'factor') {
            const factor = this.state.secondFactors.find((f) => f.alias === event.target.value);
            this.setState({selectedFactorObj: factor});
            console.log(factor.alias);
            console.log(factor.id);
        }
    }

    handleFirstPhaseSubmit(event) {
        event.preventDefault();

        const params = new URLSearchParams();
        params.append('login', this.state.login);
        params.append('password', this.state.password);
        Axios.post(config.privateApiUrl + 'auth', params)
            .then(response => {
                if (response.data.success === 1) {
                    this.setState({personId: response.data.return.personId})
                    this.setState({loginToken: response.data.return.securityToken.token})

                    const activeSecondFactors = response.data.return.secondFactors.filter(e => e.isEnabled && e.type !== 'SMS');
                    this.setState({secondFactors: activeSecondFactors});
                    if (response.data.return.secondFactors.length > 0) {
                        this.setState({selectedFactorObj: response.data.return.secondFactors[0]});
                        this.setState({selectedFactor: response.data.return.secondFactors[0].alias});
                    }
                    NotificationsHelper.getInstance().showSuccessNotification("Well done", "Congratulations, you are almost inside");
                    this.setState({loginPhase: '2fa'});
                } else {
                    console.log("Couldn't sign you in" + response.data.error);
                    this.setState({login: ''});
                    this.setState({password: ''});
                    NotificationsHelper.getInstance().showErrorNotification("Oh, no... =(", "Could not sign you in: " + response.data.error);
                }
            })
            .catch(error => {
                console.log(error.response)
            });
    }

    async handleSecondPhaseSubmit(event) {
        event.preventDefault();

        await SecondFactorService.requestConfirmationToken(
            this.state.personId,
            this.state.selectedFactorObj.id,
            'BACKOFFICE_SECOND_PHASE',
            '',
            (token) => {
                this.setState({secondFactorToken: token});
            },
            (error) => {
                NotificationsHelper.getInstance().showErrorNotification("Oh, no... =(", "Could not sign you in: " + error);
            },
            () => { /* HTTP request error callback */
            });

        await SecondFactorService.submitSecondFactorToken(
            this.state.personId,
            this.state.secondFactorToken,
            this.state.selectedFactorObj,
            this.state.code,
            (token) => {
                this.setState({secondFactorToken: token});
            },
            (error) => {
                NotificationsHelper.getInstance().showErrorNotification("Oh, no... =(", "Could not sign you in: " + error);
            },
            () => { /* HTTP request error callback */
            });

        const params = new URLSearchParams();
        params.append('personId', this.state.personId);
        params.append('loginToken', this.state.loginToken);
        params.append('secondFactorToken', this.state.secondFactorToken);
        Axios.post(config.privateApiUrl + 'finalizeAuth', params)
            .then(response => {
                if (response.data.success === 1) {
                    Cookies.set('email', this.state.login);
                    Cookies.set('access_token', response.data.return);
                    // TODO $rootScope.$broadcast("loggedIn", {});

                    DictionariesHelper.getInstance().initDictionaries();

                    this.setState({redirect: true});
                } else {
                    console.log("finalizeAuth failed: " + response.data.error);
                    this.setState({code: ''});
                    NotificationsHelper.getInstance().showErrorNotification("Oh, no... =(", "Could not sign you in: " + response.data.error);
                }
            })
            .catch(error => {
                console.log(error.response)
            });
    }

    render() {
        const {redirect} = this.state;
        if (redirect) {
            return <Redirect to='/'/>;
        }

        const factorListItems = this.state.secondFactors.map((factor) =>
            <option key={factor.id} value={factor.alias}>{factor.alias}</option>
        );

        let secondFactorForm = <p>Error occurred</p>;
        if (this.state.selectedFactorObj) {
            if (!this.state.selectedFactorObj.isEnabled) {
                secondFactorForm = <p>This factor is not supported in BackOffice</p>
            } else if (this.state.selectedFactorObj.type === 'GA') {
                secondFactorForm = <input type="text" className="form-control form-control-lg" placeholder="GA Code"
                                          name="code" value={this.state.code} onChange={this.handleChange} required/>
            } else if (this.state.selectedFactorObj.type === 'WEBAUTHN') {
                secondFactorForm = <p>Ready to use your key?</p>
            }
        }

        let signInForm;
        if (this.state.loginPhase === 'login') {
            signInForm = <form className="margin-bottom-0" onSubmit={this.handleFirstPhaseSubmit}>
                <div className="form-group m-b-20">
                    <input type="text" className="form-control form-control-lg" placeholder="Email Address"
                           name="login" value={this.state.login} onChange={this.handleChange} required/>
                </div>
                <div className="form-group m-b-20">
                    <input type="password" className="form-control form-control-lg" placeholder="Password"
                           name="password" value={this.state.password} onChange={this.handleChange} required/>
                </div>
                <div className="login-buttons">
                    <button type="submit" className="btn btn-success btn-block btn-lg">Sign me in</button>
                </div>
            </form>;
        } else {
            signInForm = <form className="margin-bottom-0" onSubmit={this.handleSecondPhaseSubmit}>
                <div className="form-group m-b-20">
                    <select className="form-control form-control-lg"
                            name="factor" onChange={this.handleChange}>
                        {factorListItems}
                    </select>
                </div>
                <div className="form-group m-b-20">
                    {secondFactorForm}
                </div>
                <div className="login-buttons">
                    <button type="submit" className="btn btn-success btn-block btn-lg">Sign me in</button>
                </div>
            </form>
        }

        return (
            <React.Fragment>
                <ReactNotification/>
                <div className="login-cover">
                    <div className="login-cover-image"
                         style={{backgroundImage: 'url(' + this.state.activeBg + ')'}}></div>
                    <div className="login-cover-bg"></div>
                </div>

                <div className="login login-v2">
                    <div className="login-header">
                        <div className="brand">
                            <span className="logo"></span> <b>DSX</b> BackOffice
                        </div>
                    </div>
                    <div className="login-content" id="login-form">
                        {signInForm}
                    </div>
                </div>

                <ul className="login-bg-list clearfix">
                    <li className={(this.state.bg1 ? 'active ' : '')}><Link to="/user/login-v2"
                                                                            onClick={(e) => this.selectBg(e, 'bg1', '/assets/img/login-bg/login-bg-17.jpg')}
                                                                            style={{backgroundImage: 'url(/assets/img/login-bg/login-bg-17.jpg)'}}></Link>
                    </li>
                    <li className={(this.state.bg2 ? 'active ' : '')}><Link to="/user/login-v2"
                                                                            onClick={(e) => this.selectBg(e, 'bg2', '/assets/img/login-bg/login-bg-16.jpg')}
                                                                            style={{backgroundImage: 'url(/assets/img/login-bg/login-bg-16.jpg)'}}></Link>
                    </li>
                    <li className={(this.state.bg3 ? 'active ' : '')}><Link to="/user/login-v2"
                                                                            onClick={(e) => this.selectBg(e, 'bg3', '/assets/img/login-bg/login-bg-15.jpg')}
                                                                            style={{backgroundImage: 'url(/assets/img/login-bg/login-bg-15.jpg)'}}></Link>
                    </li>
                    <li className={(this.state.bg4 ? 'active ' : '')}><Link to="/user/login-v2"
                                                                            onClick={(e) => this.selectBg(e, 'bg4', '/assets/img/login-bg/login-bg-14.jpg')}
                                                                            style={{backgroundImage: 'url(/assets/img/login-bg/login-bg-14.jpg)'}}></Link>
                    </li>
                    <li className={(this.state.bg5 ? 'active ' : '')}><Link to="/user/login-v2"
                                                                            onClick={(e) => this.selectBg(e, 'bg5', '/assets/img/login-bg/login-bg-13.jpg')}
                                                                            style={{backgroundImage: 'url(/assets/img/login-bg/login-bg-13.jpg)'}}></Link>
                    </li>
                    <li className={(this.state.bg6 ? 'active ' : '')}><Link to="/user/login-v2"
                                                                            onClick={(e) => this.selectBg(e, 'bg6', '/assets/img/login-bg/login-bg-12.jpg')}
                                                                            style={{backgroundImage: 'url(/assets/img/login-bg/login-bg-12.jpg)'}}></Link>
                    </li>
                </ul>
            </React.Fragment>
        )
    }
}

export default withRouter(LoginV2);